import csv
import json

data = []
with open('Test Data.csv') as infile:
    data = infile.readlines()
valid_headers = [4,5,6,7,8]
header_arr = data[0].rstrip().split(',')
del data[0]

options = []
for line in data:
    option = {}
    arr = line.rstrip().split(',')
    option['key'] = arr[0]
    option['name'] = arr[0]
    option['description'] = arr[0] + ' desc'
    option['values'] = {}
    for header in valid_headers:
        if arr[header] != '.' and arr[header] != 'NA':
            option['values'][header_arr[header]] = float(arr[header])
    options.append(option)
opt_str = json.dumps(options)
with open('Data.json', 'w') as outfile:
    outfile.write(opt_str)
